FROM alpine:latest AS builder

RUN apk --no-cache add openjdk17

WORKDIR /tmp

COPY ./ ./

RUN --mount=type=cache,target=/root/.m2 ./mvnw clean package -DskipTests

FROM eclipse-temurin:17.0.10_7-jre

WORKDIR /app

COPY --from=builder /tmp/target/spring-*.jar ./spring-petclinic.jar

EXPOSE 8080

CMD [ "java", "-jar", "/app/spring-petclinic.jar" ]